import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {

  constructor(protected http: HttpClient) { }

  getProducts() {
    return this.http.get('https://fvwzxk56cg.execute-api.us-east-1.amazonaws.com/mock/productos').pipe(map(response => response));
  }
}
