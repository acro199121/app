import { Component, OnInit } from '@angular/core';
import { products } from '../products';
import { ProductServiceService } from '../product-service.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products = products;
  users;

  constructor(
    protected productService: ProductServiceService
  ) {
  }

  ngOnInit() {
    console.log('entro');
    this.productService.getProducts()
    .subscribe(
      (data) => { // Success
        this.users = data;
        console.log(data)
      },
      (error) => {
        console.error(error);
      }
    );
  }

  share() {
    window.alert('The product has been shared!');
  }

}
